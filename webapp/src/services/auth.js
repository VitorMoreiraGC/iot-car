import Amplify, { Auth } from 'aws-amplify';
import { getConfig } from '../configuration/config';

/**
 * Initialise amplify object
 */
export function initialize() {
  const config = getConfig();
  Amplify.configure({
    Auth: {
      region: config.AWS.Region,
      identityPoolId: config.AWS.Cognito.IdpId,
      userPoolId: config.AWS.Cognito.UserPoolId,
      userPoolWebClientId: config.AWS.Cognito.UserPoolClientId,
      mandatorySignIn: false,
    },
  });
}

/**
 * Logins a user using amplify library
 * @param {String} username Username
 * @param {String} password Password
 */
export async function login(username, password) {
  try {
    const user = await Auth.signIn(username, password);
    return { error: null, data: user };
  } catch (error) {
    console.error('error:', error);
    return { error };
  }
}

/**
 * Logout the current user using amplify library
 */
export async function signOut() {
  try {
    await Auth.signOut();
  } catch (error) {
    console.error('error signing out: ', error);
  }
}

/**
 * Checks if the user is authenticated
 * Return true if it is.
 */
export async function isAuthenticated() {
  return await Auth.currentAuthenticatedUser()
    .then(() => Promise.resolve(true))
    .catch(() => Promise.resolve(false));
}
