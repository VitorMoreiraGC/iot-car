import Amplify, { PubSub } from 'aws-amplify';
import { getConfig } from '../configuration/config';
import { AWSIoTProvider } from '@aws-amplify/pubsub/lib/Providers';

export function initialise() {
  const config = getConfig();
  console.log(
    `wss://${config.AWS.IoTCore.MqttId}.iot.${config.AWS.Region}.amazonaws.com/mqtt`
  );
  Amplify.addPluggable(
    new AWSIoTProvider({
      aws_pubsub_region: config.AWS.Region,
      aws_pubsub_endpoint: `wss://${config.AWS.IoTCore.MqttId}.iot.${config.AWS.Region}.amazonaws.com/mqtt`,
    })
  );
}

export function publish(topic, data) {
  console.log('topic', topic, 'data', data);
  return PubSub.publish(topic, data);
}
