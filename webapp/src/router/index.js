import { createRouter, createWebHistory } from 'vue-router';
import Login from '../views/Login.vue';
import Drive from '../views/Drive.vue';
import { isAuthenticated } from '../services/auth';

const routes = [
  {
    path: '/',
    name: 'Root',
    component: Login,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/drive',
    name: 'Drive',
    component: Drive,
  },
  {
    path: '/*',
    name: 'Default',
    component: Login,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next) => {
  const logged = await isAuthenticated();
  if (to.name !== 'Login' && !logged) next({ name: 'Login' });
  else next();
});

export default router;
