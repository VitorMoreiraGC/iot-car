export function getConfig() {
  return {
    AWS: {
      Region: 'eu-west-2',
      Cognito: {
        IdpId: 'eu-west-2:bb030c68-cc50-4e6f-af72-9a573a50dfd3',
        UserPoolId: 'eu-west-2_9FOtEVUxz',
        UserPoolClientId: '7tqfrb2dn2sq2revkn243gu2tj',
      },
      IoTCore: {
        MqttId: 'a3p0dd75j3lt8a-ats',
      },
    },
  };
}
