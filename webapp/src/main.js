import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { initialize as authInit } from './services/auth';
import { initialise as iotInit } from './services/iot-core';
import Toaster from '@meforma/vue-toaster';

authInit();
iotInit();

createApp(App)
  .use(router)
  .use(Toaster, {
    position: 'top-right',
    duration: 4000,
  })
  .mount('#app');
