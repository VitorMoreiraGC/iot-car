#!/usr/bin/env bash
set -e

rootDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd ../ > /dev/null 2>&1 && pwd )"
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $scriptDir  
source ./cf-deploy.sh 
source ./helper.sh
cd - > /dev/null

# Full deployment of an existing system
# @Parameter 1 - [Environment] - string containing the environment intials Values:[ dev, preview, production ]
full_deployment() {
    local outputs_file='./.temp_output.yaml'
    local environment=${1}
    get_aws_credentials $environment
    local region=$AWS_DEFAULT_REGION

    #Deploy base template
    deploy_cloudformation $environment 'base' 'iotcar-base' || exit 1
    local deploymentBucket=$(cat ${outputs_file} | yq -r '.[] | select(.OutputKey=="DeploymentBucket") | .OutputValue')

    # Deploy application template
    deploy_cloudformation $environment 'applications' 'iot-car-applications' ${deploymentBucket} || exit 1

    rm -f $outputs_file
}
