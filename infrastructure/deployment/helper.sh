#!/usr/bin/env bash
set -e

GREEN="\e[32m"
RED='\033[0;31m'
YELLOW='\033[0;33m'
NC="\033[0m"

# Loads and validates the aws credential - Supports multi-environment and also local credentials if the env is empty
#
# @Parameter 1- [Environment] - string containing the environment description Allowed Values [ dev, uat, qa, prod ]
get_aws_credentials() { 
    local env=${1,,}
    error=0

    export AWS_ACCESS_KEY_ID=$(name="${env:+${env^^}_}AWS_ACCESS_KEY_ID" && echo ${!name})
    export AWS_SECRET_ACCESS_KEY=$(name="${env:+${env^^}_}AWS_SECRET_ACCESS_KEY" && echo ${!name})
    export AWS_DEFAULT_REGION=$(name="${env:+${env^^}_}AWS_DEFAULT_REGION" && echo ${!name})

    if [ -z "$AWS_ACCESS_KEY_ID" ]; then echo -e "${YELLOW}WARNING: No AWS_ACCESS_KEY_ID found for ${env:-local} environment.${NC}"; fi 
    if [ -z "$AWS_SECRET_ACCESS_KEY" ]; then echo -e "${YELLOW}WARNING: No AWS_SECRET_ACCESS_KEY found for ${env:-local} environment.${NC}"; fi 
    if [ -z "$AWS_DEFAULT_REGION" ]; then ((error+=1)) && echo -e "${RED}ERROR: No AWS_DEFAULT_REGION found for ${env:-local} environment.${NC}"; fi 

    if [ $error -gt 0 ]; then
        echo -e "${GREEN}Tip: Please check if you have variables defined as the following: ${env:+${env^^}_}AWS_ACCESS_KEY_ID.${NC}"
        exit 1
    fi
}
