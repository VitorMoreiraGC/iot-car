#!/usr/bin/env bash
set -e
rootDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd ../ > /dev/null 2>&1 && pwd )"

# Applies the stack policy recursively to the nested stacks.
#
# @Parameter 1 - [Parent Stack Name] - Parent cloudformation stack name
# @Parameter 2 - [Stack policy file path] - Stack policy file location
apply_stack_policy_to_nested_stacks() {
    local parent_stack=$1 
    local stack_policy_file=$2
    local nested_stacks=$(aws cloudformation list-stack-resources \
        --stack-name ${parent_stack} \
        --query "StackResourceSummaries[?ResourceType=='AWS::CloudFormation::Stack'].[PhysicalResourceId]" \
        --output text )
    
    for stack in ${nested_stacks}; do
        aws cloudformation set-stack-policy --stack-name ${stack} --stack-policy-body file://${stack_policy_file}
        apply_stack_policy_to_nested_stacks ${stack} ${stack_policy_file} || exit 1
    done
}

# Deploys the cloudformation to the desired environment.
#
# @Parameter 1 - [Environment] - string containing the environment description Allowed Values [ dev, uat, qa, prod ]
# @Parameter 2 - [Folder To Deploy] - Folder to be deployed containing the cloudformation templates and parameters.
# @Parameter 3 - [Stack sufix] - Stack sufix that will be appended to environment and region to form the stack name.
deploy_cloudformation() {
    local green="\e[32m"
    local red='\033[0;31m'
    local yellow='\033[0;33m'
    local nc="\033[0m"

    local env=${1,,}
    local folder=${2}
    local stackSufix=${3}
    local packagesBucket=${4:-'base'}
    local tempOutputFile=${5:-'./.temp_output.yaml'}

    local region=$AWS_DEFAULT_REGION
    local stackName="${env}-${region}-${stackSufix}"
    local templateDir=${rootDir}/${folder}/templates
    local parameterDir=${rootDir}/${folder}/parameters
    local policyPath=${rootDir}/${folder}/stack-policy.json

    echo -e "${green}Starting cloudformation deployment named (${stackName}) for ${env} environment.${nc}"

    sam package \
        --region ${region} \
        --template-file ${templateDir}/template.yaml \
        --output-template-file ${templateDir}/packaged.yaml \
        --s3-bucket $packagesBucket || exit 1

    sam deploy \
        --region ${region} \
        --stack-name ${stackName} \
        --no-fail-on-empty-changeset\
        --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND \
        --template-file ${templateDir}/packaged.yaml \
        --parameter-overrides $(cat ${parameterDir}/${env}_parameters.yaml | yq -r '.[] | .Key + "=" + .Value') || exit 1

    if [ -f ${policyPath} ]; then
        aws cloudformation set-stack-policy --stack-name ${stackName} --stack-policy-body file://${policyPath} || exit 1
        apply_stack_policy_to_nested_stacks $stackName $policyPath || exit 1
    else
      echo -e "${yellow}WARNING: Stack policy not found for ${stackName})! Advised to have one.${nc}"
    fi

    aws cloudformation describe-stacks \
        --region ${region} \
        --stack-name ${stackName} \
        --query Stacks[0].Outputs \
        --output yaml > $tempOutputFile || exit 1

    rm -f ${templateDir}/packaged.yaml
}

