#!/usr/bin/env bash
set -e

scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $scriptDir  
source ./deploy.sh 
source ./helper.sh
cd - > /dev/null

confirmation_prompt() {
    echo -e "\033[0;31m${1}\033[0m"
    proceed=0
    while [[ $proceed -ne 1 ]] ;
    do
        read -r -p "Are you sure? [y/n]" response
        case "$response" in
        [yY][eE][sS] | [yY]) proceed=1 ;;
        [nN][oO] | [nN]) exit 0 ;;
        esac
    done
}

start_deployment() {
    export DEV_AWS_DEFAULT_REGION='eu-west-2'
    local id=$(aws sts get-caller-identity --query Account --output text)
    local alias=$(aws iam list-account-aliases --query AccountAliases[0] --output text)

    echo -e "Environment [default: dev]: \c"
    read environment

    environment=${environment:-"dev"}
    confirmation_prompt "Deployment starting on account ${alias} (${id}) and environment as ${environment}.\n"
        
    full_deployment $environment
    exit 0
}

start_deployment

