#! /usr/bin/python3
from time import sleep
from Connectors.MQTTConnector import Subscribe
from Config.Config import Config
from Handler.Handler import onMessage

def main ():
  print("starting the code.")
  Subscribe('/drive', Config, onMessage)
  
main()