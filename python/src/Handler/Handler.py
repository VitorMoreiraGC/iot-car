from Drive.Drive import Drive
from Drive.EDriveAction import EDriveAction
from Motor.EMotorSpeed import EMotorSpeed
import json

def onMessage(topic, payload):
	message = json.loads(payload)

	direction = message['direction']
	print('direction {}'.format(direction))
	if(direction == "forward"): return Drive(EDriveAction.FORWARD, EMotorSpeed.HIGH)
	if(direction == "backward"): return Drive(EDriveAction.BACKWARD, EMotorSpeed.HIGH)
	if(direction == "right"): return Drive(EDriveAction.RIGHT, EMotorSpeed.HIGH)
	if(direction == "left"): return Drive(EDriveAction.LEFT, EMotorSpeed.HIGH)
	if(direction == "stop"): return Drive(EDriveAction.STOP)	




