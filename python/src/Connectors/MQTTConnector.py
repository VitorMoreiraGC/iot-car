import json
import threading
import os
from awsiot import mqtt_connection_builder
from awscrt import io, mqtt

mqtt_connection: mqtt.Connection = None

def Subscribe(topic, config, on_message):
    print('subscribing to topic: ' + topic)
    global mqtt_connection

    subscription_complete_event = threading.Event()

    try:
        _connect(config)
        mqtt_connection.subscribe(
            topic=topic,
            qos=mqtt.QoS.AT_LEAST_ONCE,
            callback=on_message)

        subscription_complete_event.wait()    
    except Exception as e:
        subscription_complete_event.set()
        mqtt_connection = None
        raise e


def _connect(config):
    global mqtt_connection
    if mqtt_connection: return mqtt_connection
    print('Opening MQTT connection to: {}'.format(config.bokerEndpoint))

    dirname = os.path.dirname(__file__)
    event_loop_group = io.EventLoopGroup(1)
    host_resolver = io.DefaultHostResolver(event_loop_group)
    client_bootstrap = io.ClientBootstrap(event_loop_group, host_resolver)

    mqtt_connection = mqtt_connection_builder.mtls_from_path(
        endpoint=config.bokerEndpoint,
        client_id=config.clientId,
        ca_filepath= os.path.join(dirname, config.rootCA),
        cert_filepath=os.path.join(dirname, config.cert),
        pri_key_filepath=os.path.join(dirname, config.privateKey),
        client_bootstrap=client_bootstrap,
        clean_session=False
    )

    mqtt_connection.connect().result()
    print("Connected!")
	
