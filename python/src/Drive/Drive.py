from Motor.MotorList import MotorList
from Motor.EMotorPosition import EMotorPosition
from Motor.EMotorSpeed import EMotorSpeed
from Motor.EMotorDirection import EMotorDirection
from Drive.EDriveAction import EDriveAction

def Drive(direction:EDriveAction, speed:EMotorSpeed=None):
  print ('Driving....{}{}', direction, speed)
  driveActions = {
    EDriveAction.STOP: lambda: __stop(),
    EDriveAction.FORWARD: lambda: __forward(speed),
    EDriveAction.BACKWARD: lambda: __backward(speed),
    EDriveAction.LEFT: lambda: __left(speed),
    EDriveAction.RIGHT: lambda: __right(speed) 
  }
  
  driveActions[direction]()

def __stop():
  for keys in MotorList.keys():
    MotorList[keys].stop()

def __forward(speed):
  for key in MotorList.keys():
    MotorList[key].start(EMotorDirection.FORWARD, speed)

def __backward(speed):
  for key in MotorList.keys():
    MotorList[key].start(EMotorDirection.BACKWARD, speed)

def __left(speed):
  MotorList[EMotorPosition.FRONT_LEFT].start(EMotorDirection.BACKWARD, speed)
  MotorList[EMotorPosition.REAR_LEFT].start(EMotorDirection.BACKWARD, speed)
  MotorList[EMotorPosition.FRONT_RIGHT].start(EMotorDirection.FORWARD, speed)
  MotorList[EMotorPosition.REAR_RIGHT].start(EMotorDirection.FORWARD, speed)

def __right(speed):
  MotorList[EMotorPosition.FRONT_LEFT].start(EMotorDirection.FORWARD, speed)
  MotorList[EMotorPosition.REAR_LEFT].start(EMotorDirection.FORWARD, speed)
  MotorList[EMotorPosition.FRONT_RIGHT].start(EMotorDirection.BACKWARD, speed)
  MotorList[EMotorPosition.REAR_RIGHT].start(EMotorDirection.BACKWARD, speed)