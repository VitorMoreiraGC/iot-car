from enum import Enum

class EDriveAction(Enum):
  STOP = 1
  FORWARD = 2
  BACKWARD = 3
  LEFT = 4
  RIGHT = 5