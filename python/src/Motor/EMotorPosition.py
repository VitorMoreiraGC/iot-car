from enum import Enum

class EMotorPosition(Enum):
  FRONT_LEFT = 1
  FRONT_RIGHT = 2
  REAR_LEFT = 3
  REAR_RIGHT = 4