from Motor.Motor import Motor
from Motor.EMotorPosition import EMotorPosition

MotorList = { 
  EMotorPosition.FRONT_LEFT: Motor(EMotorPosition.FRONT_LEFT, 17, 27, 22),
  EMotorPosition.FRONT_RIGHT:Motor(EMotorPosition.FRONT_RIGHT, 23, 24, 25),
  EMotorPosition.REAR_LEFT: Motor(EMotorPosition.REAR_LEFT, 13, 19, 26),
  EMotorPosition.REAR_RIGHT:Motor(EMotorPosition.REAR_RIGHT, 20, 21, 16),
}