from enum import Enum

class EMotorDirection(Enum):
   FORWARD = 1
   BACKWARD = 2