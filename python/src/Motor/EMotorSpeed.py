from enum import Enum

class EMotorSpeed(Enum):
  LOW = 40
  MEDIUM = 70
  HIGH = 100