from Motor.EMotorDirection import EMotorDirection
from Motor.EMotorPosition import EMotorPosition
from Motor.EMotorSpeed import EMotorSpeed
import RPi.GPIO as GPIO

class Motor:
  def __init__(self, position:EMotorPosition, in1:int, in2:int, en:int):
    self.position = position
    self.in1 = in1
    self.in2 = in2
    self.en = en
    self.__setup()

  def __setup(self):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(self.in1,GPIO.OUT)
    GPIO.setup(self.in2,GPIO.OUT)
    GPIO.setup(self.en,GPIO.OUT)
    self.pulse = GPIO.PWM(self.en, 1000)
    self.pulse.start(0)

  def start(self, direction:EMotorDirection, speed:EMotorSpeed):
    self.pulse.ChangeDutyCycle(speed.value)
    if direction == EMotorDirection.FORWARD:
        GPIO.output(self.in1, GPIO.HIGH)
        GPIO.output(self.in2, GPIO.LOW)
    elif direction == EMotorDirection.BACKWARD:
        GPIO.output(self.in1, GPIO.LOW)
        GPIO.output(self.in2, GPIO.HIGH)

  def stop(self):
    GPIO.output(self.in1, GPIO.LOW)
    GPIO.output(self.in2, GPIO.LOW)