class Config(object): 
	bokerEndpoint= 'a3p0dd75j3lt8a-ats.iot.eu-west-2.amazonaws.com'
	clientId='iot-car-client'
	rootCA= '../Config/Certs/AmazonRootCA1.pem'
	cert ='../Config/Certs/7200cd24e2-certificate.pem.crt'
	privateKey ='../Config/Certs/7200cd24e2-private.pem.key'