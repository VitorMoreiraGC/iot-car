#!/usr/bin/env bash

build(){
	pyinstaller src/main.py --onefile --noconsole --path ./iot-car-env/lib/python3.8/site-packages/
}

install() { 
	python3 -m venv iot-car-env
	source ./iot-car-env/bin/activate
	pip3 install -r requirements.txt
}

start() { 
	./dist/main
}

dev() {
	python3 ./src/main.py
}

"$@"  	